﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace saraiva.conciliacao.Entities
{
    [BsonIgnoreExtraElements]
    public class Pedido
    {
        [BsonId]
        public long Id { get; set; }
        public string IdPedidoMktp { get; set; }
        public string IdPedidoSaraiva { get; set; }
        public decimal Total { get; set; }
        public string Status { get; set; }
        public DateTime DataPagamento { get; set; }
    }
}
