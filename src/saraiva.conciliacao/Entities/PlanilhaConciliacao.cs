﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace saraiva.conciliacao.Entities
{
    public class PlanilhaConciliacao
    {
        public string PedidoAmazon { get; set; }
        public string StatusAmazon { get; set; }
        public string PedidoSaraiva { get; set; }
        public decimal ValorAnymarket { get; set; }
        public string StatusAnymarket { get; set; }
        public decimal SaldoPrevisto { get; set; }
        public decimal TaxaPrevista { get; set; }
        public DateTime DataPagamento { get; set; }
        public DateTime DataConciliacao { get; set; }
    }
}

