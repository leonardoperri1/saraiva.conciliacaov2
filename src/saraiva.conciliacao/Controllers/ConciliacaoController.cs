﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using saraiva.conciliacao.Entities;
using saraiva.conciliacao.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace saraiva.conciliacao.Controllers
{
    public class ConciliacaoController : Controller
    {
        private readonly IRepository _repository;
        public ConciliacaoController(IRepository repository)
        {
            _repository = repository;
        }

        public IActionResult Index(bool sucesso = false)
        {
            if (sucesso)
                ViewBag.Sucesso = "Arquivo importado, após o processamento será enviado para o seu e-mail!";

            return View();
        }

        [HttpPost, Route("UploadFile")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            Random rnd = new Random();
            var nomeArquivo = string.Concat(rnd.Next(), "_", file.FileName);

            var planilha = ReadFile(file.OpenReadStream());

            var diretorio = SaveFile(planilha, nomeArquivo);

            var result = await _repository.SalvarDiretorio(diretorio, nomeArquivo);

            return RedirectToAction("Index", "Conciliacao", new { sucesso = result });
        }

        private List<PlanilhaAmazon> ReadFile(Stream file)
        {
            var planilhas = new List<PlanilhaAmazon>();

            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                int rowCount = worksheet.Dimension.Rows;
                int ColCount = worksheet.Dimension.Columns;
                bool bHeaderRow = true;

                for (int row = 1; row <= rowCount; row++)
                {
                    var planilhaAmazon = new PlanilhaAmazon();
                    for (int col = 1; col <= ColCount; col++)
                    {
                        if (bHeaderRow)
                        {
                            if (worksheet.Cells[row, col].Value != null)
                            {
                                if (col == 1 && worksheet.Cells[row, col].Value.ToString().ToUpper() != "PEDIDOAMAZON")
                                {
                                    planilhaAmazon.PedidoAmazon = worksheet.Cells[row, col].Value.ToString();
                                }

                                if (col == 2 && worksheet.Cells[row, col].Value.ToString().ToUpper() != "STATUSAMAZON")
                                {
                                    planilhaAmazon.StatusAmazon = worksheet.Cells[row, col].Value.ToString();
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(planilhaAmazon.PedidoAmazon))
                        planilhas.Add(planilhaAmazon);
                }
            }

            return planilhas;
        }

        private string SaveFile(List<PlanilhaAmazon> file, string nomeArquivo)
        {
            ExcelPackage package = new ExcelPackage();
            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Conciliação Amazon");

            var i = 1;
            var titulos = new String[] { "PedidoAmazon", "StatusAmazon" };
            foreach (var titulo in titulos)
            {
                worksheet.Cells[1, i++].Value = titulo;
            }

            for (int row = 2; row < file.Count + 2; row++)
            {
                worksheet.Cells[row, 1].Value = file[row - 2].PedidoAmazon;
                worksheet.Cells[row, 2].Value = file[row - 2].StatusAmazon;
            }

            worksheet.Protection.IsProtected = false;
            worksheet.Protection.AllowSelectLockedCells = false;

            var path = string.Format("F:/WebApps/ConciliacaoAmazon/Conciliacoes/{0}", nomeArquivo);
            //var path = string.Format("C:/ConciliacaoAmazon/{0}", nomeArquivo);

            package.SaveAs(new FileInfo(path));

            return path;
        }
    }
}
