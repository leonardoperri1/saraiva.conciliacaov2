﻿using Dapper;
using MongoDB.Driver;
using saraiva.conciliacao.Configuracoes;
using saraiva.conciliacao.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace saraiva.conciliacao.Repositories
{
    public class Repository : IRepository
    {
        private readonly Conexoes _conexoes;
        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<Pedido> _pedido;


        public Repository(Conexoes conexoes)
        {
            _conexoes = conexoes;
            _database = Saraiva.Framework.DataAccess_Core.Mongo.DatabaseFactory.Create(_conexoes.DbMongoMktOut);
            _pedido = _database.GetCollection<Pedido>("Pedido");
        }

        public async Task<Pedido> ObterPedidoMongo(string idPedido)
        {
            var itens = new List<Pedido>();
            var filter = Builders<Pedido>.Filter;
            var filtroIdEncomendaMktp = Builders<Pedido>.Filter.Eq(x => x.IdPedidoMktp, idPedido);

            var cursor = await _pedido.FindAsync(filtroIdEncomendaMktp);

            return cursor.FirstOrDefault();

        }
        public async Task<List<PlanilhaConciliacao>> ObterConciliacao(int processo)
        {            
            using (var connection = new SqlConnection(_conexoes.DatabaseMktOut))
            {
                var SQL = string.Format(@"SELECT * FROM ConciliacaoAmazon WHERE Processo = '{0}'", processo.ToString()) ;

                await connection.OpenAsync();

                var result = await connection.QueryAsync<PlanilhaConciliacao>(SQL) ;

                return result.ToList();
            }
        }

        public async Task<bool> SalvarDiretorio(string diretorio, string nomeArquivo)
        {
            using (var connection = new SqlConnection(_conexoes.DatabaseMktOut))
            {
                var SQL = @"INSERT INTO ArquivoImportadoAmazon
                                (Diretorio, NomeArquivo, DataImportacao, Processado) 
                            VALUES (@Diretorio,
                                    @NomeArquivo,
                                    @DataImportacao,
                                    @Processado
                                    )";

                await connection.OpenAsync();

                var result = await connection.ExecuteAsync(SQL, new
                {
                    Diretorio = diretorio,
                    NomeArquivo = nomeArquivo,
                    DataImportacao = DateTime.Now,
                    Processado = false
                });
                
                return result > 0;
            }
        }
    }
}
