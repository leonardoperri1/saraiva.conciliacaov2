﻿using saraiva.conciliacao.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace saraiva.conciliacao.Repositories
{
    public interface IRepository
    {
        Task<Pedido> ObterPedidoMongo(string idPedido);
        Task<List<PlanilhaConciliacao>> ObterConciliacao(int processo);
        Task<bool> SalvarDiretorio(string diretorio, string nomeArquivo);
    }
}
