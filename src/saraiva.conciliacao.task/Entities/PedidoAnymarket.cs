﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace saraiva.conciliacao.task.Entities
{
    public class PedidoAnymarket
    {
        public decimal Total { get; set; }
        public string Status { get; set; }
        public DateTime PaymentDate { get; set; }
    }
}
