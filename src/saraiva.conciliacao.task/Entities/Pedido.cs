﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace saraiva.conciliacao.task.Entities
{
    [BsonIgnoreExtraElements]
    public class Pedido
    {
        [BsonId]
        public long Id { get; set; }
        public string IdPedidoMktp { get; set; }
        public string IdPedidoSaraiva { get; set; }
        public decimal Total { get; set; }
        public string Status { get; set; }
        public DateTime DataPagamento { get; set; }
        public NotaFiscal NotaFiscal { get; set; }
    }
    
    [BsonIgnoreExtraElements]
    public class NotaFiscal
    {
        public string Numero { get; set; }
    }
}
