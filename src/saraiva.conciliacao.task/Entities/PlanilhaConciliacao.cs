﻿using System;

namespace saraiva.conciliacao.task.Entities
{
    public class PlanilhaConciliacao
    {
        public string PedidoAmazon { get; set; }
        public string StatusAmazon { get; set; }
        public string PedidoSaraiva { get; set; }
        public decimal ValorAnymarket { get; set; }
        public string StatusAnymarket { get; set; }
        public decimal SaldoPrevisto { get; set; }
        public decimal TaxaPrevista { get; set; }
        public DateTime DataPagamento { get; set; }
        public DateTime DataConciliacao { get; set; }
        public string NotaFiscal { get; set; }
        public int Processo { get; set; }
    }
}

