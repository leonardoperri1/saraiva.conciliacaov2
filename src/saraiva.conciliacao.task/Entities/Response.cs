﻿using System;
using System.Collections.Generic;
using System.Text;

namespace saraiva.conciliacao.task.Entities
{
    public class Response
    {
        public bool Sucesso { get; set; }
        public string Erro { get; set; }
    }
}
