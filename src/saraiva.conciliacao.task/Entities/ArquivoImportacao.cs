﻿using System;
using System.Collections.Generic;
using System.Text;

namespace saraiva.conciliacao.task.Entities
{
    public class ArquivoImportacao
    {
        public string Diretorio { get; set; }
        public string NomeArquivo { get; set; }
    }
}
