﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace saraiva.conciliacao.task.Entities
{
    public class PlanilhaAmazon
    {
        public string PedidoAmazon { get; set; }
        public string StatusAmazon { get; set; }
    }
}
