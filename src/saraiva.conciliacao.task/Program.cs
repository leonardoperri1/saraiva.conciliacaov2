﻿using Microsoft.Extensions.DependencyInjection;

namespace saraiva.conciliacao.task
{
    class Program
    {
        public static void Main(string[] args)
        {
            var services = new ServiceCollection();
            Startup.ConfigureServices(services);

            var provider = services.BuildServiceProvider();

            provider.GetService<App>().Run();
        }
    }
}
