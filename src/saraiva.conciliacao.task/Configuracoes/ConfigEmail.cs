﻿using System;
using System.Collections.Generic;
using System.Text;

namespace saraiva.conciliacao.task.Configuracoes
{
    public class ConfigEmail
    {
        public string Email { get; set; }
        public Config configEmail { get; set; }
    }
    public class Config
    {
        public string Credencial { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Smtp { get; set; }
        public int SmtpPort { get; set; }
        public bool Ssl { get; set; }

    }
}
