﻿using saraiva.conciliacao.task.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace saraiva.conciliacao.task.Services
{
    public interface IEmailService
    {
        Task<Response> EnviarEmailPlanilha(string nomeArquivo);
    }
}
