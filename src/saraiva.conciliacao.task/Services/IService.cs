﻿using saraiva.conciliacao.task.Entities;
using System.Threading.Tasks;

namespace saraiva.conciliacao.task.Services
{
    public interface IService
    {
        Task<bool> ProcessarPlanilha();
    }
}
