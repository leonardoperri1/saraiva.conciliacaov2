﻿using OfficeOpenXml;
using saraiva.conciliacao.task.Entities;
using saraiva.conciliacao.task.Helper;
using saraiva.conciliacao.task.Repositories;
using saraiva.conciliacao.task.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace saraiva.conciliacao.task
{
    public class Service : IService
    {
        private readonly IRepository _repository;
        private readonly IEmailService _emailService;

        public Service(IRepository repository, IEmailService emailService)
        {
            _repository = repository;
            _emailService = emailService;
        }

        public async Task<bool> ProcessarPlanilha()
        {
            Random rnd = new Random();
            var processo = rnd.Next();

            var diretorio = await _repository.ObterDiretorioImportacao();

            var arquivo = LerArquivoDiretorio(diretorio[0].NomeArquivo);

            var arquivoProcessado = await ProcessarArquivo(processo, arquivo);

            if (arquivoProcessado)
            {
                await GerarNovoArquivoEEnviarEmail(processo, diretorio[0].NomeArquivo);
            }

            await _repository.AtualizarDiretorio(diretorio[0].NomeArquivo);

            return true;
        }

        private List<PlanilhaAmazon> LerArquivoDiretorio(string nomeArquivo)
        {
            string path = string.Empty;

            //var files = Directory.GetFileSystemEntries(@"C:/ConciliacaoAmazon", nomeArquivo);
            var files = Directory.GetFileSystemEntries(@"F:/WebApps/ConciliacaoAmazon/Conciliacoes", nomeArquivo);

            foreach (var file in files)
            {
                if (file.Contains(".xlsx"))
                    path = file;
                break;
            }

            byte[] data = File.ReadAllBytes(path);

            MemoryStream zipStream = new MemoryStream(data);

            List<PlanilhaAmazon> arquivo = ReadFile(zipStream);

            return arquivo;
        }

        private List<PlanilhaAmazon> ReadFile(MemoryStream zipStream)
        {

            var planilhas = new List<PlanilhaAmazon>();

            using (ExcelPackage package = new ExcelPackage(zipStream))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                int rowCount = worksheet.Dimension.Rows;
                int ColCount = worksheet.Dimension.Columns;
                bool bHeaderRow = true;

                for (int row = 1; row <= rowCount; row++)
                {
                    var planilhaAmazon = new PlanilhaAmazon();
                    for (int col = 1; col <= ColCount; col++)
                    {
                        if (bHeaderRow)
                        {
                            if (worksheet.Cells[row, col].Value != null)
                            {
                                if (col == 1 && worksheet.Cells[row, col].Value.ToString().ToUpper() != "PEDIDOAMAZON")
                                {
                                    planilhaAmazon.PedidoAmazon = worksheet.Cells[row, col].Value.ToString();
                                }

                                if (col == 2 && worksheet.Cells[row, col].Value.ToString().ToUpper() != "STATUSAMAZON")
                                {
                                    planilhaAmazon.StatusAmazon = worksheet.Cells[row, col].Value.ToString();
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(planilhaAmazon.PedidoAmazon))
                        planilhas.Add(planilhaAmazon);
                }
            }

            return planilhas;

        }

        private bool WriteFile(List<PlanilhaConciliacao> file, string nomeArquivo)
        {
            ExcelPackage package = new ExcelPackage();
            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Conciliação Amazon");

            var i = 1;
            var titulos = new String[] { "PedidoAmazon", "StatusAmazon", "PedidoSaraiva", "ValorAnymarket", "StatusAnymarket", "SaldoPrevisto", "TaxaPrevista", "DataPagamento" };
            foreach (var titulo in titulos)
            {
                worksheet.Cells[1, i++].Value = titulo;
            }

            for (int row = 2; row < file.Count + 2; row++)
            {
                worksheet.Cells[row, 1].Value = file[row - 2].PedidoAmazon;
                worksheet.Cells[row, 2].Value = file[row - 2].StatusAmazon;
                worksheet.Cells[row, 3].Value = file[row - 2].PedidoSaraiva;
                worksheet.Cells[row, 4].Value = file[row - 2].ValorAnymarket;
                worksheet.Cells[row, 4].Style.Numberformat.Format = "R$ 0.00";
                worksheet.Cells[row, 5].Value = file[row - 2].StatusAnymarket;
                worksheet.Cells[row, 6].Value = file[row - 2].SaldoPrevisto;
                worksheet.Cells[row, 6].Style.Numberformat.Format = "R$ 0.00";
                worksheet.Cells[row, 7].Value = file[row - 2].TaxaPrevista;
                worksheet.Cells[row, 7].Style.Numberformat.Format = "R$ 0.00";
                worksheet.Cells[row, 8].Value = file[row - 2].DataPagamento;
                worksheet.Cells[row, 8].Style.Numberformat.Format = "dd/mm/yyyy hh:mm:ss";
            }

            worksheet.Protection.IsProtected = false;
            worksheet.Protection.AllowSelectLockedCells = false;

            //Salvar planilha e enviar e-mail com a mesma para o usuário;
            package.SaveAs(new FileInfo(string.Format("F:/WebApps/ConciliacaoAmazon/Conciliacoes/Conciliacao_ok/{0}", nomeArquivo)));
            //package.SaveAs(new FileInfo(string.Format("C:/ConciliacaoAmazon/Conciliacao_ok/{0}", nomeArquivo)));


            return true;
        }

        private async Task<bool> ProcessarArquivo(int processo, List<PlanilhaAmazon> arquivo)
        {
            var t = new TaskQueue(maxParallelizationCount: 10, maxQueueLength: arquivo.Count());

            foreach (var arq in arquivo)
                t.Queue(() => GerarConsolidado(processo, arq));

            await t.Process();

            return true;
        }

        private async Task GerarConsolidado(int processo, PlanilhaAmazon planilhaAmazon)
        {
            var pedidoMongo = await _repository.ObterPedidoMongo(planilhaAmazon.PedidoAmazon);

            if (pedidoMongo != null)
                await _repository.SalvarConciliacao(new PlanilhaConciliacao()
                {
                    Processo = processo,
                    PedidoAmazon = planilhaAmazon.PedidoAmazon,
                    PedidoSaraiva = pedidoMongo.IdPedidoSaraiva,
                    StatusAmazon = planilhaAmazon.StatusAmazon,
                    ValorAnymarket = pedidoMongo.Total,
                    StatusAnymarket = pedidoMongo.Status,
                    DataPagamento = pedidoMongo.DataPagamento,
                    SaldoPrevisto = planilhaAmazon.StatusAmazon.ToUpper() != "ORDER" ? - pedidoMongo.Total - (pedidoMongo.Total * (-0.15M)) : pedidoMongo.Total - (pedidoMongo.Total * 0.15M),
                    TaxaPrevista = planilhaAmazon.StatusAmazon.ToUpper() != "ORDER" ? pedidoMongo.Total * (-0.15M) : pedidoMongo.Total * 0.15M,
                    NotaFiscal = pedidoMongo.NotaFiscal.Numero,
                    DataConciliacao = DateTime.Now
                });
        }

        private async Task GerarNovoArquivoEEnviarEmail(int processo, string nomeArquivo)
        {
            var conciliacoes = await _repository.ObterConciliacao(processo);

            if (conciliacoes.Count > 0)
                WriteFile(conciliacoes, nomeArquivo);

            var responseEmail = await _emailService.EnviarEmailPlanilha(nomeArquivo);

            Console.WriteLine("Enviar email: {0}",responseEmail.ToString());
        }


    }
}
