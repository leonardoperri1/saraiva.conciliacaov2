﻿using saraiva.conciliacao.task.Configuracoes;
using saraiva.conciliacao.task.Entities;
using Saraiva.Framework.Service.Email;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace saraiva.conciliacao.task.Services
{
    public class EmailService : IEmailService
    {
        private readonly ConfigEmail _configuracoes;

        public EmailService(ConfigEmail configuracoes)
        {
            _configuracoes = configuracoes;
        }

        public async Task<Response> EnviarEmailPlanilha(string nomeArquivo)
        {
            var path = string.Empty;
            var files = Directory.GetFileSystemEntries(@"F:/WebApps/ConciliacaoAmazon/Conciliacoes/Conciliacao_ok", nomeArquivo);
            //var files = Directory.GetFileSystemEntries(@"C:/ConciliacaoAmazon/Conciliacao_ok/", nomeArquivo);


            foreach (var file in files)
            {
                if (file.Contains(".xlsx"))
                    path = file;
                break;
            }

            byte[] data = File.ReadAllBytes(path);

            var conteudo = new List<EnvioArquivoByte>();

            conteudo.Add(new EnvioArquivoByte
            {
                Conteudo = data,
                Formato = ".xlsx",
                NomeArquivo = nomeArquivo
            });


            var response = new Response();
            var servico = Saraiva.Framework.Service.Email.EmailService.Instance;


            try
            {
                var assunto = String.Format("Planilha conciliação Amazon - {0}", DateTime.Now);
                var destinatarios = new List<string>();
                destinatarios.Add("terc.leonardo.oliveira@saraiva.com.br");

                var emailEnviado = await servico.EnviarEmail(destinatarios, assunto, "Olá, Segue planilha conciliação", conteudo, true);

                if (emailEnviado)
                {
                    response.Sucesso = emailEnviado;
                }
                else
                {
                    response.Sucesso = emailEnviado;
                    response.Erro = "Problema ao enviar email";
                }
            }
            catch (Exception ex)
            {
                response.Sucesso = false;
                response.Erro = string.Format("Problema ao enviar email: {0}", ex.Message);
            }

            return response;
        }
    }
}
