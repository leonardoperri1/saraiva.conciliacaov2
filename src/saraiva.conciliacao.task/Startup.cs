﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using saraiva.conciliacao.task.Configuracoes;
using saraiva.conciliacao.task.Repositories;
using saraiva.conciliacao.task.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace saraiva.conciliacao.task
{
    public static class Startup
    {
        public static IConfiguration Configuration;

        public static void ConfigureServices(IServiceCollection services)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            Configuration = builder.Build();

            #region [IOC]
            services.AddScoped<IService, Service>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IRepository, Repository>();
            services.AddTransient<App>();
            #endregion

            #region [CONNECTIONS]
            services.Configure<Conexoes>(Configuration.GetSection("Conexoes"));
            services.AddScoped(cfg => cfg.GetService<IOptionsSnapshot<Conexoes>>().Value);

            services.Configure<ConfigEmail>(Configuration.GetSection("ConfigEmail"));
            services.AddScoped(cfg => cfg.GetService<IOptionsSnapshot<ConfigEmail>>().Value);
            #endregion
        }
    }
}
