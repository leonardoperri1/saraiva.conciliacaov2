﻿using saraiva.conciliacao.task.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace saraiva.conciliacao.task.Repositories
{
    public interface IRepository
    {
        Task<Pedido> ObterPedidoMongo(string idPedido);
        Task<bool> SalvarConciliacao(PlanilhaConciliacao conciliacao);

        Task<List<PlanilhaConciliacao>> ObterConciliacao(int processo);
        Task<List<ArquivoImportacao>> ObterDiretorioImportacao();
        Task AtualizarDiretorio(string arquivo);
    }
}
