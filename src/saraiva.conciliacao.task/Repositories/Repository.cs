﻿using Dapper;
using MongoDB.Driver;
using saraiva.conciliacao.task.Configuracoes;
using saraiva.conciliacao.task.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace saraiva.conciliacao.task.Repositories
{
    public class Repository : IRepository
    {
        private readonly Conexoes _conexoes;
        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<Pedido> _pedido;

        public Repository(Conexoes conexoes)
        {
            _conexoes = conexoes;
            _database = Saraiva.Framework.DataAccess_Core.Mongo.DatabaseFactory.Create(_conexoes.DbMongoMktOut);
            _pedido = _database.GetCollection<Pedido>("Pedido");
        }

        public async Task<Pedido> ObterPedidoMongo(string idPedido)
        {
            var itens = new List<Pedido>();
            var filter = Builders<Pedido>.Filter;
            var filtroIdEncomendaMktp = Builders<Pedido>.Filter.Eq(x => x.IdPedidoMktp, idPedido);

            var cursor = await _pedido.FindAsync(filtroIdEncomendaMktp);

            return cursor.FirstOrDefault();
        }

        public async Task<bool> SalvarConciliacao(PlanilhaConciliacao conciliacao)
        {
            using (var connection = new SqlConnection(_conexoes.DatabaseMktOut))
            {
                var SQL = @"INSERT INTO ConciliacaoAmazon
                                (PedidoAmazon, StatusAmazon, PedidoSaraiva, ValorAnymarket, StatusAnymarket, TaxaPrevista, SaldoPrevisto, DataPagamento, DataConciliacao, Processo) 
                            VALUES (@PedidoAmazon,
                                    @StatusAmazon,
                                    @PedidoSaraiva,
                                    @ValorAnymarket,
                                    @StatusAnymarket,
                                    @TaxaPrevista,
                                    @SaldoPrevisto,
                                    @DataPagamento,
                                    @DataConciliacao,
                                    @Processo)";

                await connection.OpenAsync();

                var result = await connection.ExecuteAsync(SQL, new
                {
                    conciliacao.PedidoAmazon,
                    conciliacao.StatusAmazon,
                    conciliacao.PedidoSaraiva,
                    conciliacao.ValorAnymarket,
                    conciliacao.StatusAnymarket,
                    conciliacao.TaxaPrevista,
                    conciliacao.SaldoPrevisto,
                    conciliacao.DataPagamento,
                    conciliacao.DataConciliacao,
                    conciliacao.Processo
                });
            }

            return true;
        }

        public async Task<List<PlanilhaConciliacao>> ObterConciliacao(int processo)
        {            
            using (var connection = new SqlConnection(_conexoes.DatabaseMktOut))
            {
                var SQL = string.Format(@"SELECT * FROM ConciliacaoAmazon WHERE Processo = '{0}'", processo.ToString()) ;

                await connection.OpenAsync();

                var result = await connection.QueryAsync<PlanilhaConciliacao>(SQL) ;

                return result.ToList();
            }
        }

        public async Task<List<ArquivoImportacao>> ObterDiretorioImportacao()
        {
            using (var connection = new SqlConnection(_conexoes.DatabaseMktOut))
            {
                var SQL = string.Format(@"SELECT TOP 1 Diretorio, NomeArquivo FROM ArquivoImportadoAmazon WHERE Processado = 0;");

                await connection.OpenAsync();

                var result = await connection.QueryAsync<ArquivoImportacao>(SQL);

                return result.ToList();
            }
        }

        public async Task AtualizarDiretorio(string arquivo)
        {
            using (var connection = new SqlConnection(_conexoes.DatabaseMktOut))
            {
                var SQL = string.Format(@"UPDATE ArquivoImportadoAmazon
                                                        SET Processado = 1
                                                        WHERE NomeArquivo = '{0}';", arquivo);

                await connection.OpenAsync();

                var result = await connection.QueryAsync(SQL);
            }
        }
    }
}
