﻿using saraiva.conciliacao.task.Entities;
using saraiva.conciliacao.task.Services;
using System.Threading.Tasks;

namespace saraiva.conciliacao.task
{
    public class App
    {
        private readonly IService _service;

        public App(IService service)
        {
            _service = service;
        }

        public void Run()
        {
            _service.ProcessarPlanilha().Wait();
        }
    }
}
